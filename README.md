# Microcap Wine Tests

This test suite is for running automated functionality tests for MicroCap on your device.

## Setup you python environment

I like [direnv](https://github.com/direnv/direnv/wiki/Python) for this.

pip install -r requirements.txt
Tests (basic use cases)

on a fresh install of Mint20, I needed to install a few extra packages:
apt install python3-tk python3-dev
pip3 install python-dotenv

## Try a first test

Note: this testing library relies on image recognition.  Since it involves clicking on buttons whose appearance will change based on your OS, you will probably need to create your own snapshots for images if you are not using the same OS as me (Mint20).  
In the event of collaborators, I'm willing to migrate to something more generic such as GNOME.

Anyhoo, navigate to a graphical file explorer where you can see the MC12 icon you intend to launch.

Now enter:
	nosetests test/startup_tests.py:StartupTest

If all is set up correctly, pyautogui should open MC12, and then close it again and reset the icon.

If those things didn't happen, then please consult the errors to see what may have gone wrong.