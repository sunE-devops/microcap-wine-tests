import pyautogui, cv2, os

import time, distro, os

from BaseTestCase import BaseTestCase

MAX_WAIT = 30

import logging
pil_logger = logging.getLogger('PIL')
pil_logger.setLevel(logging.INFO)

class BugTest(BaseTestCase):
	# if distro.linux_distribution(full_distribution_name=False)[0]:
	prefix = 'test/snapshots/' + distro.linux_distribution(full_distribution_name=False)[0] + '/'

# The user opens MC12 by doubleclicking the icon
	def setUp(self):
		if self.check_for_main_window():
			pass
		else:
			self.deactivate_mc12_icon()
			self.launch_mc12()

	def tearDown(self):
		self.close_error_window()

	def close_error_window(self):
		close_button_loc = self.locate_snapshot('close_button', True)
		if close_button_loc != None:
			pyautogui.click(close_button_loc)

	def check_for_main_window(self):
		self.main_window_loc = self.locate_snapshot('main_window_inactive')
		if self.main_window_loc != None:
			pyautogui.click(self.main_window_loc)
			pyautogui.move(0,150)
		else:
			return False
		# assert self.main_window_loc != None, "main_window doesn't appear to be open"

	def create_new_file(self):
		pyautogui.hotkey('ctrl', 'n')
		time.sleep(0.5)
		pyautogui.hotkey('enter')
		time.sleep(0.5)


	def test_bug_50741_crash_on_add_text(self):
		self.create_new_file()

		pyautogui.hotkey('ctrl', 't')
		pyautogui.click()

		time.sleep(1)

		crash_window = self.locate_snapshot('crash_window', False)
		assert crash_window == None, "as per bug report, this action is not supposed to trigger a crash"
		

if __name__ == '__main__':  
    unittest.main()