from BaseTestCase import BaseTestCase

import pyautogui, cv2, os
from PIL import Image

import time, distro, os

MAX_WAIT = 30


class ShutdownTest(BaseTestCase):

# The user opens MC12 by doubleclicking the icon
	def setUp(self):
		self.check_for_open_mc12()
		
	def test_shutting_down(self):
		self.close_mc12()
		self.deactivate_mc12_icon()


if __name__ == '__main__':  
    unittest.main()