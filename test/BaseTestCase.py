import unittest

import pyautogui, time, distro, os
from PIL import Image

class BaseTestCase(unittest.TestCase):
	if os.name == 'nt':
		prefix = 'test/snapshots/windows/'
	elif distro.linux_distribution(full_distribution_name=False)[0] == 'linuxmint':
		prefix = 'test/snapshots/' + distro.linux_distribution(full_distribution_name=False)[0] + '/'
	else:
		raise 'currently this test suite only handles mint and windows 10'

	def launch_mc12(self):
		mc12_icon_loc = self.locate_snapshot('mc12_icon', True, 0.85)
		pyautogui.doubleClick(mc12_icon_loc)
		time.sleep(3)
		# maximize_mc12_icon_loc = self.locate_snapshot('mc12_maximize_icon')
		# pyautogui.click(maximize_mc12_icon_loc)

	def deactivate_mc12_icon(self):
		active_mc12_icon_loc = self.locate_snapshot('mc12_icon_active',None,confidence=0.85)
		if active_mc12_icon_loc == None:
			active_mc12_icon_loc = self.locate_snapshot('mc12_icon_active2',None, 0.85)		

		if active_mc12_icon_loc:
			pyautogui.moveTo(active_mc12_icon_loc)
			pyautogui.move(100, 0)
			pyautogui.click()
		else:
			pyautogui.hotkey('right') #this is a dodgy patch because the active icon changes appearance when I try to screen shot it

	def check_for_main_window(self):
		self.main_window_loc = self.locate_snapshot('main_window', True)
		assert self.main_window_loc != None, "main_window not launched"

	
	def locate_snapshot(self, path, present = '', confidence=0.9):
		fullpath = self.prefix + path + '.png'
		snapshot = Image.open(fullpath)
		loc = pyautogui.locateOnScreen(snapshot, confidence)
		if present == True:
			assert loc != None, path + " not found"
		elif False:
			assert loc == None, path + " wasn't supposed to be found, but it was"
		return loc

	def close_mc12(self):
		# in case mouse is blocking close area
		pyautogui.move(-200, 200)
		if self.locate_snapshot('mc12_close_section_workspace'):
			mc12_close_section_loc = self.locate_snapshot('mc12_close_section_workspace')
			pyautogui.moveTo(mc12_close_section_loc)
			pyautogui.move(50,-50)
		elif self.locate_snapshot('mc12_close_section_no_workspace'):
			mc12_close_section_loc = self.locate_snapshot('mc12_close_section_no_workspace')
			pyautogui.moveTo(mc12_close_section_loc)
			pyautogui.move(50,-70)
		pyautogui.click()
		mc12_close_section_workspace_loc = self.locate_snapshot('mc12_close_section_workspace', False)
		assert mc12_close_section_workspace_loc == None, "tried to click the close button, but it appears as though the mouse missed. please check the offset coordinates in the move command above"

	def check_for_open_mc12(self):
		if self.locate_snapshot('mc12_close_section_no_workspace'):
			self.close_mc12()
			time.sleep(0.25)
