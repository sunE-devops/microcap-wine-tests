from BaseTestCase import BaseTestCase

MAX_WAIT = 30


class StartupTest(BaseTestCase):

# The user opens MC12 by doubleclicking the icon
	def setUp(self):
		self.check_for_open_mc12()
		self.deactivate_mc12_icon()
		self.launch_mc12()
		self.check_for_main_window()

	def tearDown(self):
		self.close_mc12()
		self.deactivate_mc12_icon()

	def test_getting_started(self):
		print("Finish the test!")
		

if __name__ == '__main__':  
    unittest.main()