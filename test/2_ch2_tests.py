from BaseTestCase import BaseTestCase

import pyautogui, cv2, os, unittest

import time, distro, os

MAX_WAIT = 30

import logging
pil_logger = logging.getLogger('PIL')
pil_logger.setLevel(logging.INFO)


class StartupTest(BaseTestCase):
	# if distro.linux_distribution(full_distribution_name=False)[0]:
	prefix = 'test/snapshots/' + distro.linux_distribution(full_distribution_name=False)[0] + '/'

# The user opens MC12 by doubleclicking the icon
	def setUp(self):
		if self.check_for_main_window():
			self.check_for_and_close_active_project()
		else:
			self.deactivate_mc12_icon()
			self.launch_mc12()

	def tearDown(self):
		self.close_active_project()

	def check_for_and_close_active_project(self):
		mc12_active_project_section_loc = self.locate_snapshot('mc12_close_active_project_section')
		if mc12_active_project_section_loc != None:
			self.close_active_project

	def close_active_project(self):
		mc12_close_active_project_section_loc = self.locate_snapshot('mc12_close_active_project_section', True)
		pyautogui.moveTo(mc12_close_active_project_section_loc)
		pyautogui.move(40,-10)
		pyautogui.click()
		mc12_close_section_workspace_loc = self.locate_snapshot('mc12_close_section_workspace', True)
		if mc12_close_section_workspace_loc != None:
			pyautogui.moveTo(mc12_close_section_workspace_loc)
			pyautogui.move(60,-5)
			pyautogui.click()
		pyautogui.move(-200, 200)
		mc12_close_section_no_workspace_loc = self.locate_snapshot('mc12_close_section_no_workspace')
		if 	mc12_close_section_no_workspace_loc:
			pyautogui.moveTo(mc12_close_section_no_workspace_loc)
		assert mc12_close_section_no_workspace_loc != None, "tried to close the project button, but it appears as though the mouse missed. please check the offset coordinates in the move command above"

	def check_for_main_window(self):
		self.main_window_loc = self.locate_snapshot('main_window_inactive')
		if self.main_window_loc != None:
			pyautogui.click(self.main_window_loc)
			pyautogui.move(0,150)
		else: 
			return False
		# assert self.main_window_loc != None, "main_window doesn't appear to be open"

	def search_for_and_open_file(self, target_name):
		pyautogui.hotkey('ctrl', 'o')
		time.sleep(0.5)
		self.open_window_loc = self.locate_snapshot('open_window', True)

		target_file_loc = self.locate_snapshot(target_name)
		scroll_right_button_loc = self.locate_snapshot('scroll_right_button', True)
		open_button_loc = self.locate_snapshot('open_button', True)
		i = 1 # a counter to eventually kill the loop if target file is not found
		while target_file_loc == None:
			i = i + 1
			if i == 20:
				raise "scrolled to the right 20 times without finding the target file"
			pyautogui.click(scroll_right_button_loc)
			target_file_loc = self.locate_snapshot(target_name)
		pyautogui.click(target_file_loc)
		pyautogui.click(open_button_loc)
		open_button_loc = self.locate_snapshot('open_button', False)

	def open_transient_window(self):
		analysis_menu_loc = self.locate_snapshot('analysis_menu', True)
		pyautogui.click(analysis_menu_loc)
		transient_button_loc = self.locate_snapshot('transient_button', True)
		pyautogui.click(transient_button_loc)
		time.sleep(0.5)

	def run_transient(self):
		run_button_loc = self.locate_snapshot('run_button', True)
		pyautogui.click(run_button_loc)

	def test_p32_transient_analysis(self):
		self.search_for_and_open_file('MIXED4')

		self.open_transient_window()
		self.run_transient()
		time.sleep(1)

		analysis_result = self.locate_snapshot('p35_analysis_result', None, 0.85)
		if analysis_result == None:
			analysis_result = self.locate_snapshot('p35_analysis_result2', None, 0.85)
		assert analysis_result != None, "the resulting window does not match the expected"

if __name__ == '__main__':  
    unittest.main()